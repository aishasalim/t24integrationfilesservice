﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace T24IntegrationFilesService
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)  
        public Service1()
        {
            InitializeComponent();
        }
      
        protected override void OnStart(string[] args)
        {
            try
            {
                WriteLogs("Service is started at " + DateTime.Now);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = Double.Parse(ConfigurationManager.AppSettings["fetchTime"].ToString()); //number in milisecinds  
                timer.Enabled = true;
            }
            catch (Exception e)
            {
                WriteLogs("Service start exception " + e.InnerException.ToString());
                WriteLogs(e.Message.ToString());
            }

        }
        protected override void OnStop()
        {
            WriteLogs("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteLogs("Service is recall at " + DateTime.Now);
            callApis();
        }
        private void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        private static void WriteLogs(String Msg)
        {

            System.IO.StreamWriter sw;
            var dirpath = ConfigurationManager.AppSettings["FolderPath"].ToString();
            var path = ConfigurationManager.AppSettings["FilePath"].ToString();

            if (!Directory.Exists(dirpath))
            {
                Directory.CreateDirectory(dirpath);
            }

            if (!File.Exists(path + DateTime.Now.ToString("MMM") + "-" + DateTime.Now.ToString("yyyy") + ".txt"))
            {
                sw = new StreamWriter(path + DateTime.Now.ToString("MMM") + "-" + DateTime.Now.ToString("yyyy") + ".txt");

            }
            else
            {
                sw = File.AppendText(path + DateTime.Now.ToString("MMM") + "-" + DateTime.Now.ToString("yyyy") + ".txt");
            }
            sw.WriteLine(Msg + "\n");

            sw.Close();

        }

        private void WriteFilesApi()
        {
            WriteLogs("WriteFilesApi");
            try
            {

                var apiBaseUrl = ConfigurationManager.AppSettings["IntegrationsBaseUrl"].ToString();
                var url = apiBaseUrl + ConfigurationManager.AppSettings["WriteFilesApi"].ToString();

                var webrequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                webrequest.Method = "GET";
                webrequest.ContentType = "application/json";
                webrequest.ContentLength = 0;

                var response = (HttpWebResponse)webrequest.GetResponse();

            }
            catch (Exception ex)
            {
                WriteLogs(ex.InnerException.ToString());
                WriteLogs(ex.Message.ToString());
            }

        }

        private void callReadResponseApi()
        {
            WriteLogs("callReadResponseApi");
            try
            {

                var apiBaseUrl = ConfigurationManager.AppSettings["IntegrationsBaseUrl"].ToString();
                var url = apiBaseUrl + ConfigurationManager.AppSettings["ReadResponseApi"].ToString();

                var webrequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
                webrequest.Method = "GET";
                webrequest.ContentType = "application/json";
                webrequest.ContentLength = 0;

                var response = (HttpWebResponse)webrequest.GetResponse();

            }
            catch (Exception ex)
            {
                WriteLogs(ex.InnerException.ToString());
                WriteLogs(ex.Message.ToString());
            }

        }

        private void callApis()
        {
            callReadResponseApi();
            WriteFilesApi();
        }
    }
}
